from django.shortcuts import render, redirect
# from django.http import HttpResponse
# from django.template import loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.forms.models import model_to_dict

# Create your views here.
from .models import GroceryItem

def index(request):
	groceryitem_list = GroceryItem.objects.all()
	context = {'groceryitem_list': groceryitem_list}
	return render(request, "django_practice/index.html", context)


def groceryitem(request, groceryitem_id):
	groceryitem = model_to_dict(GroceryItem.objects.get(pk=groceryitem_id))
	# response = "You are viewing the details of %s"
	return render(request, "django_practice/groceryitem.html", groceryitem)


def register(request):
	users = User.objects.all()

	is_user_registered = False

	context = {
		"is_user_registered": is_user_registered
	}

	for indiv_user in users:
		if indiv_user.username == "johndoe":
			is_user_registered = True
			break

	if is_user_registered == False:
		user_to_register = User()
		user_to_register.username = "johndoe"
		user_to_register.first_name = "John"
		user_to_register.last_name = "Doe"
		user_to_register.email = "john@mail.com"

		user_to_register.set_password("john123")
		user_to_register.is_staff = False
		user_to_register.is_active = True

		user_to_register.save()
		
		context = {
			"first_name" : user_to_register.first_name,
			"last_name": user_to_register.last_name
		}

	return render(request, "django_practice/register.html", context)

def change_password(request):

	is_user_authenticated = False
	user = authenticate(username = "johndoe", password = "john123")
	print(user)

	# Code here executes if the user successfully authenticated
	if user is not None:
		authenticated_user = User.objects.get(username='johndoe')
		authenticated_user.set_password("johndoe1")
		authenticated_user.save()
		is_user_authenticated = True

	context = {
			"is_user_authenticated" : is_user_authenticated
		}

	return render(request, "django_practice/change_password.html", context)		

def login_view(request):

    username = "johndoe"
    password = "johndoe1"

    user = authenticate(username=username, password=password)

    context = {
        "is_user_authenticated": False
    }

    print(user)

    if user is not None:
        # Saves the user’s ID in the session using Django's session framework
        login(request, user)
        return redirect("index")
    else:
        return render(request, "django_practice/login.html", context)

def logout_view(request):
	# Removes the saved data session upon login
    logout(request)
    return redirect("index")	